const bodyParser = require("body-parser");
const morgan = require("morgan");
const express = require("express");
const app = express();
const resRoutes = require("./routes/reservation");
const hotelRoutes = require("./routes/hotel");

const cors = require("cors");
///Development logging
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// parse requests of content-type - application/json
app.use(bodyParser.json());
app.use(cors());

app.use("/api/v1/reservation", resRoutes);
app.use("/api/v1/hotel", hotelRoutes);

module.exports = app;
