const Res = require("../models/reservation");
const Hotel = require("../models/hotel");

exports.createReservation = async (req, res) => {
  try {
    //check the hotel validity
    const hotel = await Hotel.findById(req.body.hotel);
    console.log(hotel);
    if (hotel.isValid) {
      const reservation = await Res.create(req.body);
      reservation.save();
      res.status(200).json({
        status: "success",
        data: {
          reservation,
        },
      });
    } else {
      res.status(200).json({
        status: "success",
        message: "hotel not disponible",
      });
    }
  } catch (err) {
    res.status(404).json({
      status: "fail",
      err,
    });
  }
};
//get all reservations
exports.getReservations = async (req, res) => {
  try {
    let reservations = await Res.find();
    res.status(200).json({
      status: "success",
      result: reservations.length,
      data: {
        reservations,
      },
    });
  } catch (err) {
    res.status(404).json({
      status: "fail",
      err,
    });
  }
};
