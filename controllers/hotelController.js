const Hotel = require("../models/hotel");

exports.createHotel = async (req, res) => {
  try {
    const hotel = await Hotel.create(req.body);
    hotel.save();
    res.status(200).json({
      status: "success",
      data: {
        hotel,
      },
    });
  } catch (err) {
    res.status(404).json({
      status: "fail",
      err,
    });
  }
};
exports.checkHotelValidity = async (req, res) => {
  try {
    let id = req.params.idHotel;
    const hotel = await Hotel.findById(id);

    if (hotel.isValid === true) {
      res.status(200).json({
        status: "success",
        message: "hotel disponible",
        isValid: true,
      });
    } else {
      res.status(200).json({
        status: "success",
        message: "hotel non disponible",
        isValid: false,
      });
    }
  } catch (err) {
    res.status(404).json({
      status: "fail",
      err,
    });
  }
};
//get all hotels
exports.getHotels = async (req, res) => {
  try {
    let hotels = await Hotel.find({ Status: "open" });
    res.status(200).json({
      status: "success",
      result: hotels.length,
      data: {
        hotels,
      },
    });
  } catch (err) {
    res.status(404).json({
      status: "fail",
      err,
    });
  }
};
