const express = require("express");
const hotelController = require("../controllers/hotelController");
const router = express.Router();

router.route("/").get(hotelController.getHotels);
router.route("/").post(hotelController.createHotel);
router.route("/:idHotel").post(hotelController.checkHotelValidity);

module.exports = router;
