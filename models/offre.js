const mongoose = require("mongoose");

const offreSchema = new mongoose.Schema({
  Title: {
    type: String,
  },
  nbPersons: {
    type: String,
  },
  createAt: {
    type: Date,
    default: Date().now,
  },
  name: {
    type: String,
  },
  roomNumber: {
    type: Number,
  },
});

const Offre = mongoose.model("Offre", offreSchema);
module.exports = Offre;
