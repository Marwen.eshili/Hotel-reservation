const mongoose = require("mongoose");

const resSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  phone: {
    type: String,
  },
  email: {
    type: String,
  },
  description: {
    type: String,
  },
  hotel: {
    type: mongoose.Schema.ObjectId,
    ref: "Hotel",
  },

  createAt: {
    type: Date,
    default: Date().now,
  },
  roomNumber: {
    type: Number,
  },
});

const Res = mongoose.model("Res", resSchema);
module.exports = Res;
