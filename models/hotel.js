const mongoose = require("mongoose");

const hotelSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  rate: {
    type: String,
  },
  createAt: {
    type: Date,
    default: Date().now,
  },
  gov: {
    type: String,
  },
  rooms: {
    type: Number,
  },
  isValid: {
    type: Boolean,
  },
});

const Hotel = mongoose.model("Hotel", hotelSchema);
module.exports = Hotel;
